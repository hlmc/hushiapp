package com.hlmc.controller;

import com.hlmc.dao.ShopDao;
import com.hlmc.entity.Page;
import com.hlmc.entity.Shop;
import com.hlmc.service.Shop_Service;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class ShopController {

    @Resource
    private Shop_Service shop_service;

    @RequestMapping("/addShop")
    public void AddShop(Shop shop){
        shop_service.addShop(shop);
    }
    @RequestMapping("/delShop")
    public void DelShop(int id){
        shop_service.delShop(id);
    }
    @RequestMapping("/editShop")
    public void EditShop(Shop shop){
        shop_service.editShop(shop);
    }

    @ResponseBody
    @RequestMapping("/findShop")
    public  Shop findShopById(int id){
        Shop shop=new Shop();
        shop=shop_service.findShopById(id);
        return shop;
    }
    @ResponseBody
    @RequestMapping("/findUserShop")
    public  List<Shop> findShopByUid(String uid){
        List<Shop> shops=new ArrayList<Shop>();
        shops=shop_service.findAllByUid(uid);
        return shops;
    }
    @ResponseBody
    @RequestMapping("/findShopPage")
    public List<Shop> findShopPage(@RequestParam(defaultValue = "1") int page){

        Map<String,Integer> map=new HashMap<String, Integer>();
        int pageSize=12;
        int totalRecord=shop_service.findCount();
        Page<Shop> coverPage = new Page<Shop>(pageSize, page, totalRecord, null);
        int start = (coverPage.getCurrentPage() - 1) * pageSize;
        map.put("start", start);
        map.put("size", pageSize);
        List<Shop> shops=new ArrayList<Shop>();
        shops=shop_service.findAll(map);
        return shops;
    }

}
