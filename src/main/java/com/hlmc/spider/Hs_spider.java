package com.hlmc.spider;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;

public class Hs_spider extends BaseProcessor{
    private Site site = Site.me().setRetryTimes(3).setSleepTime(100).setCharset("utf-8" + "");

    @Override
    public void process(Page page) {
        page.addTargetRequest("http://www.hbnu.edu.cn/");
        String image=page.getHtml().xpath("//div[@class='focus']/img/@src").all().toString().replace("[","").replace("]","").replace(" ", "");
        System.out.println(image);
    }

    @Override
    public Site getSite() {
        return site;
    }

    public static void main(String[] args) {

        Spider.create(new Hs_spider()).addUrl("http://www.hbnu.edu.cn/")
                .thread(5).run();

    }
}
