package com.hlmc.service;

import com.hlmc.dao.ShopDao;
import com.hlmc.entity.Shop;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service("shop_service")
public class Shop_Service implements ShopDao {

    @Resource
    private ShopDao shop_dao;

    @Override
    public int addShop(Shop shop) {
        shop_dao.addShop(shop);
        return 0;
    }

    @Override
    public int delShop(int id) {
        shop_dao.delShop(id);
        return 0;
    }

    @Override
    public int editShop(Shop shop) {
        shop_dao.editShop(shop);
        return 0;
    }

    @Override
    public Shop findShopById(int id) {
        Shop shop=new Shop();
        shop=shop_dao.findShopById(id);
        return shop;
    }

    @Override
    public int findCount() {
        int total=0;
        total=shop_dao.findCount();
        return total;
    }

    @Override
    public List<Shop> findAllByUid(String uid) {
        List<Shop> shops=new ArrayList<Shop>();
        shops=shop_dao.findAllByUid(uid);
        return shops;
    }

    @Override
    public List<Shop> findAll(Map<String ,Integer> map) {
        List<Shop> shops=new ArrayList<Shop>();
        shops=shop_dao.findAll(map);
        return shops ;
    }
}
