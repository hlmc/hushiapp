package com.hlmc.dao;

import com.hlmc.entity.Shop;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component("shop_dao")
public interface ShopDao {

    int addShop(Shop shop);

    int delShop(int id);

    int editShop(Shop shop);

    Shop findShopById(int id);

    int findCount();

    List<Shop> findAllByUid(String uid);

    List<Shop> findAll(Map<String,Integer> map);//查询最新商品，按时间排序分页

}
